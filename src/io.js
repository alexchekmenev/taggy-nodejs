const socketio = require('socket.io');
let io = null;

module.exports = {
    init: (server) => {
        io = socketio(server);
        io.on('connection', function(client) {
            console.log('Client connected...');

            client.on('join', function(data) {
                console.log('join', data);
            });

            client.on('upload', function (data) {
                console.log('upload', data);
                client.broadcast.emit('upload', data);
            });

            client.on('disconnect', function() {
                console.log('disconnect');
            });
        });
    },
    get: () => {
        return io;
    }
};