

const client = require('redis').createClient();
const Q = require('q');

client.on('error', function (err) {
    console.log('Error ' + err);
});

function saveUpload(upload) {
    upload.t = +new Date();
    const value = JSON.stringify(upload);
    const deferred = Q.defer();
    client.lpush('uploads', value, (err, res) => {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(res);
        }
    });
    return deferred.promise;
}

function getRecentUploads(K) {
    const deferred = Q.defer();
    client.lrange('uploads', 0, K, (err, res) => {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(res);
        }
    });
    return deferred.promise;
}

function getStats() {
    const deferred = Q.defer();
    client.hgetall('stats', (err, res) => {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(res);
        }
    });
    return deferred.promise;
}
module.exports = {
    saveUpload,
    getRecentUploads,
    getStats
};