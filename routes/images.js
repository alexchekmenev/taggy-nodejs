var express = require('express');
var router = express.Router();
var multer = require('multer');
const rp = require('request-promise');
const path = require('path');
const Q = require('q');
const {saveUpload} = require('../src/uploads');
const LIMIT = 5;

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        // console.log('file saved to ' + path.join(process.cwd(), '../images'));
        cb(null, path.join(process.cwd(), '../images'))
    },
    filename: function(req, file, cb) {
        const parts = file.originalname.split('.');
        const prefix = parts.slice(0, parts.length - 1).join('.');
        cb(null, prefix + '-' + Date.now() + (parts.length > 0 ? '.' + parts[parts.length - 1] : ''));
    }
});
const upload  = multer({ storage: storage });

router.post('/', upload.single('image'), function(req, res, next) {
  // console.log(req.file);
  const options = {
      method: 'GET',
      uri: 'http://localhost:8081',
      qs: {
          filename: req.file.filename
      },
      json: true
  };
  return rp(options).then(result => {
      if (result.hasOwnProperty('result')) {
          const items = Object.values(result['result'].reduce((prev, item) => {
              if (!prev.hasOwnProperty(item.tag)) {
                  prev[item.tag] = item;
              }
              return prev;
          }, {})).slice(0, LIMIT);
          return Q.all(items.map(item => {
              return translateTag(item.tag).then(tagRu => {
                  // item.tag_ru = tagRu;
                  // item.score = +(item.score * 100).toFixed(2);
                  return {
                      tag_en: item.tag.replace('_', ' '),
                      tag_ru: tagRu,
                      score: +(item.score * 100).toFixed(2)
                  };
              });
          })).then(results => {
              const response = {
                  filename: req.file.filename,
                  tags: results
              };
              const io = require('../src/io').get();
              io.sockets.emit('upload', response);
              return saveUpload(response).then(_ => res.json(response));
          }).catch(err => {
              console.error(err);
          });
      } else if (result.hasOwnProperty('error')) {
          return res.json(result);
      }

  });
});

function translateTag(tag) {
    const options = {
        method: 'POST',
        url: 'https://translate.yandex.net/api/v1.5/tr.json/translate',
        headers:
            {   'Cache-Control': 'no-cache',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
        form:
            { key: 'trnsl.1.1.20140927T191415Z.85ed08b03e3d84ae.c9561739ed7a76ef198ecb09eef98cceb9d102be',
                lang: 'en-ru',
                text: tag ? tag.replace('_', ' ') : '',
                format: 'plain'
            },
        json: true
    };
    return rp(options).then(result => {
        if (result.text && result.text.length > 0) {
            return result.text[0];
        } else {
            return null;
        }
    }).catch(err => {
        console.error(err);
    });
}

module.exports = router;
