var express = require('express');
var router = express.Router();
const {getRecentUploads, getStats} = require('../src/uploads');
const LIMIT = 10;

router.get('/', function(req, res, next) {
    getRecentUploads(LIMIT).then(uploads => {
        console.log(uploads);
        res.json(uploads.map(JSON.parse));
    });
});

router.get('/stats', function(req, res, next) {
    getStats().then(stats => {
        console.log(stats);
        res.json(stats);
    });
});

module.exports = router;
