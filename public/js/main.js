
$('#image-form').on('submit', function (e) {
    e.preventDefault();
    sendImage(this);
});

function sendImage(form) {
    toggleProgress();
    $.ajax({
        type: 'POST',
        url: '/images',
        data: new FormData(form),
        processData: false,
        contentType: false,
        cache: false,
        success: function (data) {
            var content = '<div class="imagecloud"><img src="/images/' + data.filename + '" height="150"></div>';
            content += data.tags.reduce(function (prev, item) {
                if (prev.length !== 0) {
                    prev += '<br>';
                }
                prev += '<div class="tagcloud"><code>#' + item.tag_ru + '(' + item.tag_en + ') - ' + item.score + '%</code></div>';
                return prev;
            }, '');
            setUploads(content);
            toggleProgress();
        },
        error: function (err) {
            // console.error(err);
            setUploads(JSON.stringify(err));
            toggleProgress();
        }
    });
}

function setUploads(result) {
    $('#output').html(result);
}

function toggleProgress() {
    $('#progress').toggle();
}

var socket = io.connect('http://146.185.141.83:8080');
socket.on('connect', function(data) {
    socket.emit('join', 'Hello World from client');
    getRecentUploads(function () {
        console.log('recent uploads ok');
    });
});
socket.on('upload', function (data) {
    // $('#recent-results').append(JSON.stringify(data));
    getRecentUploads(function () {
        console.log('recent uploads ok');
    });
});


function getRecentUploads(cb) {
    $.ajax({
        type: 'GET',
        url: '/uploads',
        success: function (uploads) {
            var content = '';

            uploads.forEach(data => {
                content += '<div style="display: block;padding-bottom: 100px;"><div class="imagecloud"><img src="/images/' + data.filename + '" height="150"></div>';
                content += data.tags.reduce(function (prev, item) {
                    if (prev.length !== 0) {
                        prev += '<br>';
                    }
                    prev += '<div class="tagcloud"><code>#' + item.tag_ru + ' (' + item.tag_en + ') - ' + item.score + '%</code></div>';
                    return prev;
                }, '');
                content += '</div>';
            });

            setUploads(content);
            cb();
        },
        error: function (err) {
            console.error(err);
            setUploads(JSON.stringify(err));
        }
    });
}

